Yapılacak küçük web application’da;
- Bir file upload sayfası bulunmalıdır ve ekteki json dosyası verildiğinde, dosya içeriğindeki bilgileri database’e insert etmelidir. 
- Başka bir sayfada, database’deki bu dataları, her sayfada en fazla 50 adet record olacak şekilde listelenmelidir.
- Database yapısı json dosyasındaki field’lara bakılarak oluşturulmalıdır.
- Herhangi bir framework kullanımı tercih sebebidir.
- Yapılan application’ı çalıştırmak için gerekli talimatlar belirtilmelidir.
- PHP 5.4 ve üzerinde çalışabilmelidir.
