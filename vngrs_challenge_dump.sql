-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Anamakine: localhost
-- Üretim Zamanı: 08 Haz 2014, 01:49:14
-- Sunucu sürümü: 5.6.12-log
-- PHP Sürümü: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Veritabanı: `vngrs_challenge`
--
CREATE DATABASE IF NOT EXISTS `vngrs_challenge` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `vngrs_challenge`;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `objects`
--

CREATE TABLE IF NOT EXISTS `objects` (
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `releaseYear` year(4) NOT NULL,
  `alias` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `details` text COLLATE utf8_unicode_ci NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `timezoneType` tinyint(4) NOT NULL COMMENT 'timezone type',
  `timezone` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `accessory` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `brand` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `model` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `groupName` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `groupHprice` smallint(6) NOT NULL,
  `groupDprice` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
