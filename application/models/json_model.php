<?php 
/*
* Json_Model { Json parsing and upload json file }
*/
class json_model extends CI_Model{

	public function __construct()
	{	
		parent::__construct();
		$this->load->helper(array('url','form','file'));
		$this->load->library(array('curl'));
		$this->load->model(array('db_model'));
	}
	
	public function upload($_file)
	{
		$config['upload_path'] = 'libs/uploads/';
		
		$this->load->library('upload',$config);
		
		$this->upload->initialize($config);
		
		$this->upload->set_allowed_types('*');
		
		if($this->upload->do_upload($_file)):
		
			$data = array('upload' => $this->upload->data());
			$file_name = $data['upload']['file_name'];
			
			return $file_name;

		endif;
		
		return false;
	}
	
	public function get($_file)
	{
		header('Content-Type: text/plain; charset="UTF-8"');
		$read = $this->curl->simple_get('libs/uploads/'.$_file);
		$json = json_decode($read);
		return $json;
	}
	
	public function parse($_file)
	{
		foreach($_file as $json):
		$this->db_model->insert(
			$json->name,
			$json->releaseYear,
			$json->alias,
			$json->details,
			$json->createDate->date,
			$json->createDate->timezone_type,
			$json->createDate->timezone,
			$json->accessory,
			$json->brand,
			$json->model,
			$json->group->name,
			$json->group->hourlyPrice,
			$json->group->dailyPrice
		);
		endforeach;
	}
	
}