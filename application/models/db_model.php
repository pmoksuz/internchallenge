<?php 
/*
* DB_Model
*/
class DB_Model extends CI_Model{

	public function __construct()
	{	
		parent::__construct();
	}
	
	public function insert($name,$releaseYear,$alias,$details,$date,$zonetype,$timezone,$accessory,$brand,$model,$groupName,$groupHprice,$groupDprice)
	{
		$data = array(
			'name' => $name,
			'releaseYear' 	=> $releaseYear,
			'alias'		  	=> $alias,
			'details'	  	=> $details,
			'date'		  	=> $date,
			'timezoneType'	=> $zonetype,
			'timezone'	  	=> $timezone,
			'accessory'	  	=> $accessory,
			'brand'		  	=> $brand,
			'model'		  	=> $model,
			'groupName'   	=> $groupName,
			'groupHprice' 	=> $groupHprice,
			'groupDprice' 	=> $groupDprice
		);
		
		$this->db->insert('objects',$data);
	}
	
	public function fetchAll($per,$page)
	{
		$this->db->limit($per,$page);
		$_q = $this->db->get('objects');
		
		if($_q->num_rows()>0):
			
			foreach($_q->result() as $row):
			$data[] = $row;
			endforeach;
			
			return $data;
		endif;
		return false;
	}
	
	public function recordCount()
	{
		return $this->db->count_all('objects');
	}
	
	public function init($_dbName)
	{	
			$this->db->query('use '.$_dbName);
			$this->db->query("CREATE TABLE IF NOT EXISTS `objects` (
	  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
	  `releaseYear` year(4) NOT NULL,
	  `alias` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
	  `details` text COLLATE utf8_unicode_ci NOT NULL,
	  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	  `timezoneType` tinyint(4) NOT NULL COMMENT 'timezone type',
	  `timezone` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
	  `accessory` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
	  `brand` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
	  `model` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
	  `groupName` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
	  `groupHprice` smallint(6) NOT NULL,
	  `groupDprice` smallint(6) NOT NULL
	) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;");

	}
	
}