<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Intern extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
	
		$this->load->model(array('json_model','db_model'));
		$this->load->library('pagination');

			$this->db_model->init('vngrs_pmoksuz');
	}

    public function index()
    {
        $this->load->view('upload_page');
    }

    public function getForm()
    {
        $file = $this->json_model->upload('json');
		
		if($file):
		$json = $this->json_model->get($file);
		$this->json_model->parse($json);
		redirect('index.php/intern/listObjects');
		else:
		redirect('?exception');
		endif;
    }
	
	/* Test Json 
	public function getFile()
	{
		$json = $this->json_model->get('challenge.json');
		$db   = $this->json_model->parse($json);
	}
	*/
	
	public function listObjects()
	{
		$config = array();
		$config['base_url'] = base_url('index.php/intern/listObjects');
		$config['total_rows'] = $this->db_model->recordCount();
		$config['per_page'] = 10;
		$config['uri_segment'] = 3;
		
		// Pagination Styles
		$config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';

		$config['first_link'] = '&laquo;';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_link'] = '&raquo;';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_link'] = '&rarr;';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';

		$config['prev_link'] = '&larr;';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		// Pagination Styles ###
		
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		
		$data['results'] = $this->db_model->fetchAll($config['per_page'],$page);
		
		$data['links'] = $this->pagination->create_links();
		
		$this->load->view('list_page',$data);
	}

}