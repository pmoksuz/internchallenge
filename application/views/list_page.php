<html>
<head>
    <meta charset="UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <title>VNGRS CHALLENGE</title>
	<!-- Bootstrap -->
    <link href="<?=LIB_PATH;?>css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="<?=LIB_PATH;?>css/bootstrap-theme.min.css">
</head>
<body>
<div id="body">
	<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li><a href="<?=base_url();?>">Yeni Dosya Yükle</a></li>
            <li class="active"><a href="#">Tüm Verileri Listele</a></li>
          </ul>
        </div>
      </div>
    </div>

	            
<div class="container theme-showcase" role="main">
	<div class="jumbotron">
       <h3 class="pull-right"> Peker Mert Öksüz<br>pmoksuz@gmail.com<br>+905343019599</h3>
	   <ul class="pagination"><?php echo $links; ?></ul>
	   <div class="clearfix"></div>
    </div>
	<?php if($results):?>
	<div class="table-responsive">
		<table class="table table-striped">
		  <thead>
			<tr>
				<th>Name</th>
				<th>Release Year</th>
				<th>Alias</th>
				<th>Details</th>
				<th>Create Date</th>
				<th>Timezone{type/zone}</th>
				<th>Accessory</th>
				<th>Brand</th>
				<th>Model</th>
				<th>Group{N,H,D}</th>
			</tr>
		  </thead>
		  <tbody>
		<?php foreach($results as $data): ?>
		<tr>
			<th><?=$data->name;?></th>
			<th><?=$data->releaseYear;?></th>
			<th><?=$data->alias;?></th>
			<th><?=$data->details;?></th>
			<th><span class="label label-primary pull-left"><?=$data->date;?> </span></th>
			<th><span class="label label-danger pull-left"><span class="badge"><?=$data->timezoneType;?></span> |  <?=$data->timezone;?></span>
			</th>
			<th><?=$data->accessory;?></th>
			<th><?=$data->brand;?></th>
			<th><?=$data->model;?></th>
			<th><span class="label label-primary pull-left">Name: <?=$data->groupName;?></span>
				<span class="label label-success pull-left">Hourly: <?=$data->groupHprice;?> </span>
				<span class="label label-warning pull-left">Daily: <?=$data->groupDprice;?></span>
			</th>
		</tr>
		<?php endforeach;?>
		
		 </tbody>
        </table>
    </div>
	<?php else: ?>
	<div class="alert alert-warning">
      <strong>Veri Bulunamadı!</strong> Henüz veritabanına bir dosya yüklenmemiş!
    </div>
	<div class="btn-group">
		<a href="<?=base_url();?>"><button type="button" class="btn btn-primary">Dosya Yükleyin</button></a>
	</div>
	<?php endif; ?>

</div>
</body>
</html>