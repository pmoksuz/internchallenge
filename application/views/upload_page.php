<html>
<head>
    <meta charset="UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <title>VNGRS CHALLENGE</title>
	<!-- Bootstrap -->
    <link href="<?=LIB_PATH;?>css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="<?=LIB_PATH;?>css/bootstrap-theme.min.css">
</head>
<body>
<div id="body" >
	<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="<?=base_url();?>">Dosya Yükle</a></li>
            <li ><a href="<?=base_url('index.php/intern/listobjects');?>">Tüm Verileri Listele</a></li>
          </ul>
        </div>
      </div>
    </div>

	<div class="container theme-showcase" role="main">
		<div class="jumbotron">
		<h3 class="pull-right"> Peker Mert Öksüz<br>pmoksuz@gmail.com<br>+905343019599</h3>

		<div class="pull-left col-lg-8" style="margin-top:20px;" ><div class="panel panel-default">
		  <div class="panel-heading">Upload Panel</div>
		  <div class="panel-body">
			<form action="<?=base_url('index.php/intern/getForm');?>" method="POST" enctype="multipart/form-data" >
			<div class="col-lg-12 pull-left">
			<label class="pull-left">Json File : _</label>
			<input type="file" class="pull-left" name="json" />
			</div>
			<input type="submit" class="btn btn-primary pull-right"  value="Dosyayı Yükle" />
			</form>   
		  </div>
		</div></div>

			
		<div class="clearfix"></div>	
		</div>
	
	</div>

</div>
</body>
</html>