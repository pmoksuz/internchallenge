README

Proje Codeigniter çatısı altında geliştirilmiştir.
Script Kurulum ve Ayarları

- 'vngrs_pmoksuz' adında bir veritabanına gerek duyar. Manuel olarak oluşturunuz. Tablolar otomatik oluşacaktır.
- Codeigniter /application/config/ dizini altında bir kaç düzenleme gerektirir.
    1)config.php içerisindeki ilgili kısmı düzenleyin $config['base_url']   = 'http://localhost:99/vngrs/';
    2)database.php içerisinde mysql bilgilerinizi girin.
    3)constants.php içerisinde ilgili kısmı düzenleyin. define('LIB_PATH', 'http://localhost:99/vngrs/libs/bootstrap/');

Not
Veritabanı ismini değiştirmek isterseniz intern.php içerisinde construct altında db_model->init('..') parametrisini değiştirin.
